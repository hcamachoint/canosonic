<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(); ?></title><!--Cambia el title al nombre del sitio-->
    <?php wp_head();?><!--Carga los archivos css del tema-->
  </head>
  <body <?php body_class(); ?>>
<header>
<div class="top_bar">
<div class="container">
<div class="col-md-6">
<ul class="social">
<li><a target="_blank" href="https://www.facebook.com/canosonicca"><i class="fa fa-facebook text-white"></i></a></li>
<li><a target="_blank" href="https://twitter.com/CanosonicCA"><i class="fa fa-twitter text-white"></i></a></li>
<li><a target="_blank" href="https://www.instagram.com/canosonic_c.a/"><i class="fa fa-instagram text-white"></i></a></li>
<li><a target="_blank" href="https://www.linkedin.com/in/canosonic-ca-850153146/"><i class="fa fa-linkedin text-white"></i></a></li>
<!--
<li><a target="_blank" href="https://api.whatsapp.com/send?phone=580000000&text=Me%20gustaría%20saber%20el%20precio%20del%20Alquiler%20de%20equipos."><i class="fa fa-whatsapp text-white"></i></a></li>
-->
</ul></div>

<div class="col-md-6">
<ul id="hide" class="rightc">
<li><a href="mailto:info@canosonic.com"><i class="fa fa-envelope-o"></i></a> </li>
<li><a href="/contacto" ><i class="fa fa-user"></i></a></li>      
</ul>
<ul id="unhide" class="rightc">
<li><i class="fa fa-envelope-o"></i> <a href="mailto:info@canosonic.com">info@canosonic.com </a> </li>
<li><i class="fa fa-user"></i> <a href="/contacto" >Contactanos </a></li>      
</ul>
</div>
</div>
</div>
<!--top_bar-->



<nav class="navbar navbar-default" role="navigation">
      <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <?php if ( get_header_image() ) : ?><!--Permite cargar image_header-->
            <div id="site-header" style="padding-right: 10px;">
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                <img src="<?php header_image(); ?>" id="logo" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
              </a>
            </div>
          <?php else: ?>
            <div id="header"><!--Header editable: titulo, descripcion e icono-->
             <div id="headerimg">
                <a class="navbar-brand" href="<?php echo get_option('home'); ?>">
                   <?php bloginfo('name'); ?></a>
                 <div class="description">
                   <?php bloginfo('description'); ?>
                 </div>
              </div>
            </div>
          <?php endif; ?>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
        
        
        <?php
            wp_nav_menu( array( //Menu editable en wordpress
                'menu'              => 'top_menu',
                'depth'             => 2,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'nav navbar-nav navbar-right',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
          ?>

    </div><!-- /.container-fluid -->
  </nav>
   
</header>
